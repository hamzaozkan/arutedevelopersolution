﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AruteDeveloperSolution
{
    public class Company : BaseClass<Company>
    {
        public Company(string Name, Address Address)
        {
            this.Name = Name;
            this.Address = Address;
        }
        public Company()
        {
            this.Address = new Address();
        }
        public string Name { get; set; }

        public Address Address { get; set; }

        public override Company Find(Guid id)
        {
            string dosya_yolu = this.GetType() + ".json";
            List<Company> listCompany = new List<Company>();
            using (StreamReader _StreamReader = new StreamReader(dosya_yolu))
            {
                string jsonData = _StreamReader.ReadToEnd();
                listCompany = JsonConvert.DeserializeObject<List<Company>>(jsonData);

            }
            if (listCompany != null && listCompany.Count > 0)
            {
                return listCompany.Where(x => x.Id == id).FirstOrDefault();
            }
            return null;
        }

        public override void Save()
        {
            string dosya_yolu = this.GetType() + ".json";
            List<Company> list = new List<Company>();
            if (File.Exists(dosya_yolu))
            {
                using (StreamReader _StreamReader = new StreamReader(dosya_yolu))
                {
                    string jsonData = _StreamReader.ReadToEnd();
                    list = JsonConvert.DeserializeObject<List<Company>>(jsonData);
                    _StreamReader.Close();
                }
                if (list == null)
                {
                    list = new List<Company>();
                }
                this.WriteInFile(list, dosya_yolu, this, FileMode.Truncate, false);
            }
            else
            {
                this.WriteInFile(list, dosya_yolu, this, FileMode.Append, false);
            }
        }

        public void WriteInFile(List<Company> list, string dosya_yolu, Company obj, FileMode fileMode, bool isRemove = false)
        {
            //İşlem yapacağımız dosyanın yolunu belirtiyoruz.
            FileStream fs = new FileStream(dosya_yolu, fileMode, FileAccess.Write);
            //Bir file stream nesnesi oluşturuyoruz. 1.parametre dosya yolunu,
            //2.parametre dosya varsa açılacağını yoksa oluşturulacağını belirtir,
            //3.parametre dosyaya erişimin veri yazmak için olacağını gösterir.
            StreamWriter sw = new StreamWriter(fs);
            if (isRemove)
            {
                var entity = list.Where(x => x.Id == obj.Id).FirstOrDefault();
                list.Remove(entity);
            }
            else
            {
                list.Add(obj);
            }

            //Yazma işlemi için bir StreamWriter nesnesi oluşturduk.
            sw.WriteLine(JsonConvert.SerializeObject(list));
            //Dosyaya ekleyeceğimiz iki satırlık yazıyı WriteLine() metodu ile yazacağız.
            sw.Flush();
            //Veriyi tampon bölgeden dosyaya aktardık.
            sw.Close();
            fs.Close();
            //İşimiz bitince kullandığımız nesneleri iade ettik.
        }

        public override void Delete()
        {
            string dosya_yolu = this.GetType() + ".json";
            List<Company> list = new List<Company>();
            if (File.Exists(dosya_yolu))
            {
                using (StreamReader _StreamReader = new StreamReader(dosya_yolu))
                {
                    string jsonData = _StreamReader.ReadToEnd();
                    list = JsonConvert.DeserializeObject<List<Company>>(jsonData);
                    _StreamReader.Close();
                }
                if (list == null)
                {
                    list = new List<Company>();
                }
                this.WriteInFile(list, dosya_yolu, this, FileMode.Truncate, true);
            }
        }
    }
}
