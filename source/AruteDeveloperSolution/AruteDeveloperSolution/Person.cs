﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AruteDeveloperSolution
{
    public class Person : BaseClass<Person>
    {
        public Person(string FirstName, string LastName, Address Address)
        {
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.Address = Address;
        }
        public Person()
        {
            this.Address = new Address();
        }
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Address Address { get; set; }

        public override Person Find(Guid id)
        {
            string dosya_yolu = this.GetType() + ".json";
            List<Person> listPerson = new List<Person>();
            using (StreamReader _StreamReader = new StreamReader(dosya_yolu))
            {
                string jsonData = _StreamReader.ReadToEnd();
                listPerson = JsonConvert.DeserializeObject<List<Person>>(jsonData);
            }
            if (listPerson != null && listPerson.Count > 0)
            {
                return listPerson.Where(x => x.Id == id).FirstOrDefault();
            }

            return null;
        }

        public override void Save()
        {
            string dosya_yolu = this.GetType() + ".json";
            List<Person> list = new List<Person>();
            if (File.Exists(dosya_yolu))
            {
                using (StreamReader _StreamReader = new StreamReader(dosya_yolu))
                {
                    string jsonData = _StreamReader.ReadToEnd();
                    list = JsonConvert.DeserializeObject<List<Person>>(jsonData);
                    _StreamReader.Close();
                }
                if (list == null)
                {
                    list = new List<Person>();
                }
                this.WriteInFile(list, dosya_yolu, this, FileMode.Truncate, false);
            }
            else
            {
                this.WriteInFile(list, dosya_yolu, this, FileMode.Append, false);
            }
        }

        public override void Delete()
        {
            string dosya_yolu = this.GetType() + ".json";
            List<Person> list = new List<Person>();
            if (File.Exists(dosya_yolu))
            {
                using (StreamReader _StreamReader = new StreamReader(dosya_yolu))
                {
                    string jsonData = _StreamReader.ReadToEnd();
                    list = JsonConvert.DeserializeObject<List<Person>>(jsonData);
                    _StreamReader.Close();
                }
                if (list == null)
                {
                    list = new List<Person>();
                }
                this.WriteInFile(list, dosya_yolu, this, FileMode.Truncate, true);
            }
        }
        public void WriteInFile(List<Person> list, string dosya_yolu, Person obj, FileMode fileMode, bool isRemove = false)
        {
            //İşlem yapacağımız dosyanın yolunu belirtiyoruz.
            FileStream fs = new FileStream(dosya_yolu, fileMode, FileAccess.Write);
            //Bir file stream nesnesi oluşturuyoruz. 1.parametre dosya yolunu,
            //2.parametre dosya varsa açılacağını yoksa oluşturulacağını belirtir,
            //3.parametre dosyaya erişimin veri yazmak için olacağını gösterir.
            StreamWriter sw = new StreamWriter(fs);
            if (isRemove)
            {
                var entity = list.Where(x => x.Id == obj.Id).FirstOrDefault();
                list.Remove(entity);
            }
            else
            {
                list.Add(obj);
            }

            //Yazma işlemi için bir StreamWriter nesnesi oluşturduk.
            sw.WriteLine(JsonConvert.SerializeObject(list));
            //Dosyaya ekleyeceğimiz iki satırlık yazıyı WriteLine() metodu ile yazacağız.
            sw.Flush();
            //Veriyi tampon bölgeden dosyaya aktardık.
            sw.Close();
            fs.Close();
            //İşimiz bitince kullandığımız nesneleri iade ettik.
        }
    }
}
