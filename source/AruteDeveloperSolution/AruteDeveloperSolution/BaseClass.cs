﻿using System;

namespace AruteDeveloperSolution
{
    public abstract class BaseClass<T>
    {
        private Guid _id = Guid.NewGuid();
        public Guid Id { get => _id; set => _id = value; }

        public abstract T Find(Guid id);

        public abstract void Save();
        public abstract void Delete();

    }
}
