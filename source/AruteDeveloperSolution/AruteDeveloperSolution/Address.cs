﻿using System;

namespace AruteDeveloperSolution
{
    public class Address : BaseClass<Address>
    {
        public Address(string Street, string City, string Tx, string PostCode)
        {
            this.Street = Street;
            this.City = City;
            this.Tx = Tx;
            this.PostCode = PostCode;
        }
        public Address()
        {
        }
        public string Street { get; set; }

        public string City { get; set; }

        public string Tx { get; set; }

        public string PostCode { get; set; }

        public override Address Find(Guid id)
        {
            throw new NotImplementedException();
        }

        public override void Save()
        {
            throw new NotImplementedException();
        }
        public override void Delete()
        {
            throw new NotImplementedException();
        }
    }
}

